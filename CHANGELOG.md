
## 0.0.10 [08-29-2024]

deprecate the pre-built

See merge request itentialopensource/pre-built-automations/ip-fabric-compare-network-routes-from-snapshots!7

2024-08-29 19:48:22 +0000

---

## 0.0.9 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/ip-fabric-compare-network-routes-from-snapshots!6

---

## 0.0.8 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/ip-fabric-compare-network-routes-from-snapshots!5

---

## 0.0.7 [01-06-2022]

* Certify on 2021.2

See merge request itentialopensource/pre-built-automations/ip-fabric-compare-network-routes-from-snapshots!2

---

## 0.0.6 [09-22-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/ip-fabric-compare-network-routes-from-snapshots!1

---

## 0.0.5 [09-21-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/ip-fabric-compare-network-routes-from-snapshots!1

---

## 0.0.4 [09-21-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/staging/ip-fabric-compare-network-routes-from-snapshots!1

---

## 0.0.3 [09-07-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.2 [09-07-2021]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n
