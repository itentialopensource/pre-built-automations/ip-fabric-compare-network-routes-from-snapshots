## _Deprecation Notice_
This Pre-Built has been deprecated as of 09-01-2024 and will be end of life on 09-01-2025. The capabilities of this Pre-Built have been replaced by the [IP Fabric - REST](https://gitlab.com/itentialopensource/pre-built-automations/ip-fabric-rest)

<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# IP Fabric Compare Network Routes from Snapshots

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
  * [Operations Manager and JSON-Form](#operations-manager-and-json-form)
* [Requirements](#requirements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

The IP Fabric Compare Network Routes from Snapshots Pre-Built can be used to compare the last and penultimate (previous) snapshots taken by the [IP Fabric Automated Network Assurance Platform](https://ipfabric.io/product/). The Pre-Built consists of two API tasks and a comparision view task which will render the delta.
This Pre-Built can be scheduled in Operations Manager and the delta could be reviewed in IAP or exported to any other system of record.

_Estimated Run Time_: 1min

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2021.2`
  * [IP Fabric Adapter](https://gitlab.com/itentialopensource/adapters/telemetry-analytics/adapter-ipfabric)

## Requirements

This Pre-Built requires the following:

* [IP Fabric Adapter](https://gitlab.com/itentialopensource/adapters/telemetry-analytics/adapter-ipfabric) configured with credentials and IP Address of your IP Fabric Instance.

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button (as shown below).

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-Built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built:

* Run the Operations Manager item `IP Fabric Compare Network Routes from Snapshots` or call [IP Fabric Compare Network Routes from Snapshots](./bundles/workflows/IP%20Fabric%20Compare%20Network%20Routes%20from%20Snapshots) from your workflow as a child job.
<!-- Update the below line with your Pre-Built name -->
